## Introduction

This folder contains datamodels for openmdm in the atfx format.

## Model files

There are several types of atfx files here:

mdm_module_*: these are partial datamodels (moduls), meant as building blocks.
mdm_complete_application_model: a complete application model consisting of all necessary moduls.
mdm_api_application_model: a basic application model for using the mdm api
mdm_minimum_ods_application_model_500: an application model focusing on the central ods elements. Not sufficient for the API.
mdm_minimum_ods_application_model_with_templates_500: an application model focusing on the central ods elements, but also with the mdm template elements.

## Setting up an Oracle database with ascoba

Ascoba is a tool which comes with HighQSoft's Avalon, so you need an appropriate license to use it. We assume in this section
that you have an Avalon with an appropriate license available.

1. Choose if you want to start with a full model with all extensions (mdm_complete_application_model) or a model which just supports the mdm api (mdm_api_application_model).
If you choose the latter, be aware that ascoba doesn't directly support extension of models via atfx files. If you need extensions you could add the contents of the 
coresponding *module* files by hand using the extendmodel tool, or use something like Audi's odscompare, if you have that available.

2. Set up an empty Oracle instance. Grant your instance user at least the following rights:
grant create session, create table, create sequence to xxx

2. Install the Avalon. For readability, we assume in this documentatiom, that you use the layout as described in the dockerfile under the Setup Tools 
directory in this project. If you want to use the such generated docker Image directly, you may connect to it using 
docker run --net=host -ti -v /path/to/datamodels:/tmp/datamodels avalon /bin/bash
to get a shell and to make this directory accessible under /tmp/datamodels.

3. connect to the empty oracle instance, e.g. via sqlplus xxx/password@dbname. Run the SQL-script provided by your Avalon distribution in $ATHOS_ROOT/etc/mme1_ora.sql
   This should create the necessary ods meta tables.

4. edit the ascoba.ini from the ascoba subdirectory at the marked locations.

5. Point the environment variable ASCOBA_INI to your modified ascoba.ini by running
   export ASCOBA_INI=/tmp/datamodels/ascoba_import.ini

5. run 
   ascoba verbose select "*" where {"*" eq "*"} using randomstring
   
6. run 
   UpdateStorage-12_1 USERNAME PASSWORD DATABASENAME sequence
   
7. if there were no errors, you may now start your ods server.


## Setting up an Oracle database with odscompare
If you have Audi's ODSCompare available, you may also set up your database using that tool. As that tool is to our knowledge currently not publicly available
we don't provide a detailed step by step description through the process. Please see the ODSCompare documentation for help.

## Copyright and License ##
Copyright (c) 2015-2018 Contributors to the Eclipse Foundation

 See the NOTICE file(s) distributed with this work for additional
 information regarding copyright ownership.

 This program and the accompanying materials are made available under the
 terms of the Eclipse Public License v. 2.0 which is available at
 http://www.eclipse.org/legal/epl-2.0.

 SPDX-License-Identifier: EPL-2.0




