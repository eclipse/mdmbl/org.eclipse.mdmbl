<?xml version='1.0' encoding='UTF-8'?>
<!-- This document was created with Syntext Serna Free. --><!DOCTYPE topic PUBLIC "-//OASIS//DTD DITA Topic//EN" "topic.dtd" []>
<topic xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="openMDM5.business_object_model" xsi:noNamespaceSchemaLocation="urn:oasis:names:tc:dita:xsd:topic.xsd:1.1">
  <title>openMDM5 Business Object Model</title>
  <shortdesc>This section presents how the openMDM5 Business Object Model looks like and how it can be adapted to a concrete model for own business purposes.</shortdesc>
  <topic id="openMDM5.business_object_model.model_compositions">
    <title>Model Compositions</title>
    <body>
      <p>OpenMDM 5 has not one complete model, which is capable of all possible use cases. Not every application covers all features, so it is useful to provide some sort of customization. There are three types of models. Each model is based on another and extend its features. The two basic models are mostly fixed (the exceptions are explained later) and last one is customizable. The fixed models are useful, so different application entities can work on a well-defined basis.</p>
      <p>The models are:<ul>
          <li>
            <b>Minimum Model</b>
          </li>
          <li>
            <b>Default Model</b>
          </li>
          <li>
            <b>Custom Model</b>
          </li>
        </ul></p>
      <p><image href="images/Business Object Model.png" align="center"/></p>
    </body>
  </topic>
  <topic id="openMDM5.business_object_model.minimum_model">
    <title>Minimum Model</title>
    <body>
      <p>The Minimum Model contains all objects that are essentially for all openMDM applications. All the associated objects are necessary to read and write measurement data, which is the most basic core target of openMDM. </p>
      <p>Use Cases that are covered with this model:<ul>
          <li>Create Test Data</li>
          <li>Import Test Data</li>
          <li>Modify Test Data</li>
          <li>Read Test Data</li>
        </ul></p>
      <p>The model is structured in four logical parts:<ul>
          <li><b>Management Data</b>: These objects are used to put measurement data sets in a structural form. With this form it is possible to describe relations between particular records. </li>
          <li>
            <b>Context Data</b>
            <ul>
              <li><b>Measured Data </b>: If the Context Data is linked to the Measurement Data, these objects are used to describe the basic condition under which the measurement is executed. </li>
              <li><b>Ordered Data</b>:If the Context Data is linked to the Management Data, these objects are used to describe the basic condition under which the measurement shall be executed. </li>
            </ul>
          </li>
          <li><b>Measurement Data</b>: These objects are used to describe the atomic measurement data records and how they form a measurement data set </li>
          <li><b>Measurement Dimension Data</b>: These objects are used to describe in which physical or chemical dimension a measured value is quantified.</li>
        </ul></p>
      <p><image href="images/Minimum Model.png" align="center"/></p>
    </body>
    <topic id="openMDM5.business_object_model.minimum_model.management_data">
      <title>Management Data</title>
      <body>
        <section>
          <title>Test</title>
          <p>A Test bundles all information which are related to a logical test sequence.</p>
          <p>A complete Test includes all information, which incur during the openMDM process for the specification, execution and documentation of a test. are. Regarding the model, the test assembles a set of TestSteps (measuring steps). There could be linear predecessor or successor relations between these TestSteps. But each of them can only have one predecessor and one successor.</p>
          <p>During the lifecycle of a Test, more and more information can be added to the Test. While planning a test, it is possible that only a part of the TestSteps are specified. At a later date additional TestSteps can be added to the Test. This includes the addition of  not original specified TestSteps after the test execution (unplanned ad hoc measurements)</p>
        </section>
        <section>
          <title>TestStep</title>
          <p>A TestStep summarizes all information in terms of a chronological and completely defined phase of a test.</p>
          <p>All of the included information refer to this specific phase and describe them entirely. There is no dependency between single TestSteps regarding its contents.</p>
          <p>Both the specification of the TestStep before its execution and during its execution are documented separately. There is no dependency between them and they are permanently available. Within an openMDM system it is therefore always traceable, which discrepancy has occurred between plan date and execution date. It is always possible to trace, how a Test was planned orginally and how it was executed.</p>
        </section>
      </body>
    </topic>
    <topic id="openMDM5.business_object_model.minimum_model.context_data">
      <title>Context Data</title>
      <body>
        <p>The  Context Data business objects describing the complete basic condition as mentioned above and are technically the same objects. The difference lies in the content, which covers the information of a specific context. </p>
        <p>Basic conditions, under which the test is executed, is beside the actual measurement data a essential part of a test result. Only with the help of these conditions it can be decided on the reutilization of test results or their relevance for reinterpretation or the aggregation of them.  </p>
        <p>There are two characteristics of Context Data, a specification of a TestStep on the one hand  and the documentation of the MeaResult on the other (&quot;measurement description&quot;).  The context data of the TestStep is optional and describe how the measurement is ordered, this is called the &quot;measurement description&quot;. The context data of the MeaResult is mandatory and describe the measurement itself, this is called the &quot;measurement description&quot;.</p>
        <section>
          <title>UnitUnderTest</title>
          <p>A UnitUnderTest is a specific Context Data object, which is used to describe the test specimen itself. A specimen can be modeled as a hierarchy of  parts. </p>
          <p>For example if the specimen is a car, the engine, the chassis and the wheels are parts of it. The crankshaft or the piston could be part of a engine.</p>
        </section>
        <section>
          <title>TestSequence</title>
          <p>A TestSequence is a specific Context Data object, which is used to describe the  conditions of the test sequence. This covers external terms like the outdoor temperature or the gear which is used during the test run. A TestSequence can be modeled  as a hierarchy of terms.</p>
        </section>
        <section>
          <title>TestEquipment</title>
          <p>A TestEquipment is a specific Context Data object, which is used to describe the equipment, which is used for the test sequence and its configuration. This could for example be a governor setting of the test bench or a specific configuration parameter of the test suite. A TestEquipment can be modeled as a hierarchy of equipment parts. </p>
          <p>As a special set of equipment a TestEquipment can define sensors. A sensor defines a specific measuring channel. So a sensor defines which type of information should be measured. Every sensor is related to a specific quantity, unit and physical dimension as described in the ASAM ODS standard. An example for a sensor is the tire pressure on the left back tire or the ferum content of the engine oil.</p>
          <note>The Context Data is related to TestStep, where in contrast the ASAM ODS base model would relate them to specific measurement results. So the Context Data apply for all measurement results, which are part of a specific TestStep. </note>
        </section>
      </body>
    </topic>
    <topic id="openMDM5.business_object_model.minimum_model.measurement_data">
      <title>Measurement Data</title>
      <body>
        <section>
          <title>MeaResult</title>
          <p>A MeaResult (measurement result) is a set of information about the specimen, that is recorded during the test process. The test process itself is executed under the terms of the Context Data.</p>
          <p>The information content of MeaResults is conformed to the ASAM ODS standard. A MeaResult consists on a series of MeaQuantities.</p>
          <p>MeaResults are characterized by the following properties:<ul>
              <li>Physical Dimensions</li>
              <li>Quantities</li>
              <li>Units</li>
            </ul></p>
        </section>
        <section>
          <title>ResultParameter</title>
          <p>A ResultParameter is specific piece of context information for a test result.  A MeaResult can contain a set of result parameters.</p>
        </section>
        <section>
          <title>SubMatrix </title>
          <p>A SubMatrix is a container of measurement data spanned by a number of measurement rows. </p>
          <p>Up to one of these rows can be marked as the &quot;independent row&quot;. Then all of the other measurement rows are considered to depend on this row. A time based row is often used as independent row, but quantities as pressure or amperage are just as possible.</p>
        </section>
        <section>
          <title>LocalColumn</title>
          <p>A LocalColumn is a row of measurement values belonging to one specific quantity. </p>
        </section>
        <section>
          <title>MeaQuantity</title>
          <p>A MeaQuantity defines the scientific characteristics of a measurement channel. This object is the link to the &quot;Measurement Context Data&quot;.</p>
          <p>The MeaQuantity often are associated with sensors, which are described in the Context Data. These sensors can be regarded as &quot;source&quot; of the measured  values, but they have no direct relation.</p>
        </section>
      </body>
    </topic>
    <topic id="openMDM5.business_object_model.minimum_model.measurement_context_data">
      <title>Measurement Context Data</title>
      <body>
        <section>
          <title>Unit</title>
          <p>An Unit is a specific magnitude that is used as a standard of measurement. Basically what is defined by a &quot;unit of measurement&quot;, e.g. gram, metre, second, litre, pascal, etc. </p>
        </section>
        <section>
          <title>PhysDimension</title>
          <p>A PhysDimension is a property which is associated with physical quantities for purposes of classification. Basically what is defined by a &quot;physical dimension&quot;, e.g. mass, length, time, volume, pressure, etc. </p>
        </section>
        <section>
          <title>Quantity</title>
          <p>A Quantity is a property that is measured. It separates the same Unit from different use cases, e.g. air pressure, hydraulic pressure and sound pressure all have  different meanings.</p>
        </section>
      </body>
    </topic>
  </topic>
  <topic id="openMDM5.business_object_model.default_model">
    <title>Default Model</title>
    <body>
      <p>The Default Model extends the Minimum Model to provide some use cases  that are required in nearly every openMDM application. This is mainly covered by the measurement template feature and the classification feature.</p>
      <p>Use Cases that are covered with this model:<ul>
          <li>Administrate Catalog Components</li>
          <li>Administrate Component Templates</li>
          <li>Administrate TestStep Templates</li>
          <li>Administrate Test Templates</li>
          <li>Represent a Workflow for Test Data</li>
          <li>Structure Test Data</li>
        </ul></p>
    </body>
    <topic id="openMDM5.business_object_model.default_model.measurement_templatesl">
      <title>Measurement Templates</title>
      <body>
        <p>openMDM Templates are a form of descriptive pattern, by means of which a structure of Tests, TestSteps and Context Data can be defined and standardized. They are used for test planning and the verification of the conformity of test specifications and test results. </p>
        <p>Every template is developed out of a catalog. The catalog consists of components (Catalog Components) that are related to one Context Data object (UnitUnderTest, TestSequence, TestEquipment). A Catalog Component again consists of a set of context data attributes (Catalog Attributes). A attributes is basically defined by a name and a data type. In addition for each attribute a package of properties can be defined, which influence their handling within the workflow:<ul>
            <li>optional - defines, whether the attribute is required for a completely specification of the Context Data. This is valid for attributes, whose values are known after the execution of a test (e.g. weather conditions) or attributes, whose values are not known in every case. </li>
            <li>copyable - defines, whether the content of the attribute should be inherited if the Context Data is copied. Note: identifying attributes should not be copyable.</li>
            <li>sortindex - index for displaying purposes </li>
          </ul>A Catalog Sensor is a special form of a Catalog Component to describe a sensor, which was mentioned in the Minimum Model.  </p>
        <p>The templates themselves are organized a hierarchic form. The lowest layer is formed by Component Templates, which describing the characteristics of the  Context Data triple. (UnitUnderTest, TestSequence, TestEquipment). Every Component Templates consists on a set of Catalog Components, which can be modfied with limitations. So the Component Template  and its attributes (Template Attributes) are always associated with a catalog counterpart. Structurally a Component Template is formed as tree with a root object (Template Component Root). Template Components can be nested under the root or other Template Components. In addition to normal template attributes a Template Test Equipment can be associated with a set of Template Sensors. A Template Sensor has attributes as a Template Component, but has always a relation to a MeaQuanity and thereby to an Unit, a PhysDimension and a Quantity.</p>
        <p><image href="images/Measurement Templates.png" align="center"/></p>
        <p>A Template TestStep is build of exactly one Component Template for each Context Data type. A Template Test is build of a set of Template TestSteps, which can be defined as optional.  </p>
        <p>For structuring purposes the Template Group object can be used. It can be associated with a Test, TestStep or a Component template. Moreover it&apos;s possible, to nest a Template Group under another to represent a tree structure.</p>
        <p>A Template MeaResult define rules for creating measurement data, e.g. for creating threshold values, extreme values or target value graphs. This includes the Template Submatrix, wich defines a value range for the number of rows for the Submatrices that will be associate with  the appropriate MeaResult. In equivalent to Template Sensors a Template ParameterSet can be defined for Template MeaResults. In addition a Template ParameterSet can be defined for the Template Sensor. With Template ParameterSet it is possible to define default values for ResultParameters.  </p>
      </body>
    </topic>
    <topic id="openMDM5.business_object_model.default_model.management_structure">
      <title>Management Structure</title>
      <body>
        <p>The default model provides additional business objects for structuring purposes within the Management Data section. They conduce to simplify the administration and preparation of data. They contain information about usage, organizational correlation and the history of the data, but no functional content.  An other field of application is using these objects as appliance for access authority.</p>
        <note>It&apos;s important to keep the Management Structure entirely clean of functional content, which refer to the tests themselves. On the one hand Tests should contain all for them relevant information. On the other hand there is a risk of complicating the maintenance of the data or creating inconsistences, because the information is stored redundantly.</note>
        <p><image href="images/Management Structure.png" align="center"/></p>
        <section>
          <title>Project</title>
          <p>Projects serve as reference of tests to a specific subject, series, scope, ect.</p>
          <p>The form and content are not prescribed but can influence the options of access rights. Possible assignments are product development projects, practical approaches (e.g &quot;Oil Analysis&quot;) or organizational criterias (e.g. &quot;Testbench GLaDOS&quot;)</p>
        </section>
        <section>
          <title>StructureLevel</title>
          <p>A StructureLevel is an optional grouping for a Test within a Project.</p>
        </section>
      </body>
    </topic>
    <topic id="openMDM5.business_object_model.default_model.status">
      <title>Status</title>
      <body>
        <p>A test process is normally be executed according to a specific workflow. So a stateful model is necessary to fulfill a momentary traceability. The Status business object describes the current workflow state  of another business object. The stateful business objects are:<ul>
            <li>
              <b>Project</b>
            </li>
            <li>
              <b>StructureLevel</b>
            </li>
            <li>
              <b>Test</b>
            </li>
            <li>
              <b>TestStep</b>
            </li>
            <li>
              <b>MeaResult</b>
            </li>
          </ul></p>
        <p><image href="images/Status Model.png" align="center"/></p>
        <p>The following values of Status are possible:<ul>
            <li><b>Active</b>: The business object is being modified</li>
            <li><b>Canceled</b>: The request for this business object has been canceled</li>
            <li><b>Checked</b>: Business object was imported to openMDM and the data has been reviewed and validated</li>
            <li><b>Closed</b>: Locked, no data can be added to this business object</li>
            <li><b>Defining</b>: The Context Data of this business object is being defined.</li>
            <li><b>Done</b>: The process for business object has been completed and it&apos;s ready to be import in the openMDM system</li>
            <li><b>Frozen:</b> No further result data can be added to this business object.</li>
            <li><b>Imported</b>: The business object has been completely imported to the openMDM system</li>
            <li><b>Importing:</b> The business object is being currently imported to the openMDM system</li>
            <li><b>Published:</b> All contents of this business object have been reviewed and it is available to evaluation and reporting.</li>
            <li><b>Rejected</b>: The business object has been rejected by the responsible person due to any conflict.</li>
            <li><b>Released:</b> The business object has been completely described and is ready for the test process. </li>
          </ul></p>
        <note>Not every Status value must be valid for every business object. Custom rules can be defined by a system planer to map a corporate workflow.</note>
      </body>
    </topic>
  </topic>
  <topic id="openMDM5.business_object_model.custom_model">
    <title>Custom Model</title>
    <body>
      <p>A openMDM bussiness object model isn&apos;t fixed in its scope. Since openMDM is based on ASAM ODS it is extendible, taking into account the rules that ODS defines. The Default Model can therefore be extended by a custom set of modules.</p>
      <section>
        <title>Module</title>
        <p>A Module is package of features, which belong together and being self-sufficient. It is always associated with thematically similar use cases. If a system planer is interested in a Module, he can adopt it in a running system and start to using its interface in the corporate application. Every Module has to contain the following parts:<ul>
            <li>complete description of its associated use cases </li>
            <li>business object model</li>
            <li>application model (documentation and ATFX-file) and its links to the openMDM Default Model</li>
            <li>set of interfaces granting access to the features </li>
            <li>at least one default implementation for every interface (additonal alternative implementations can be added in the future)</li>
          </ul>   </p>
        <p><image href="images/Module Scope.png" align="center"/></p>
      </section>
      <section>
        <title>Existing Modules</title>
        <p>As part of openMDM 4 there are the following Modules, which may be migrated in the future:<ul>
            <li><b>Security</b>: Contains business objects used to define the status, the classification and the domain of MDM tests. </li>
            <li><b>Tags</b>: A business object, to mark Tests, TestSteps or MeaResult with a specific context information.</li>
            <li><b>ExtSystem</b>: Contains business objects for mappings between attributes in external system and MDM attributes.</li>
            <li><b>FavouriteList</b>: Contains business objects to create a collection of other business objects and relate them to a specific user.</li>
            <li><b>I18n</b>: Contains business objects for the internationalization of a openMDM model.</li>
            <li><b>ValueList</b>: Contains business objects to create String value lists to relate them with Catalog Attributes.</li>
            <li><b>NVH</b>: Contains only the official NVH physical dimension and unit instances. A full documentation of NVH is available in the ODS 5.3.0 specification. </li>
            <li><b>MDM Log</b>: Contains a business objects to persisting log messages.</li>
            <li><b>Workflow</b>: Contains business objects to represent the workflow of test processes.</li>
          </ul> </p>
      </section>
    </body>
  </topic>
</topic>
