# Release Notes - openMDM(R) Application #

* [mdmbl Eclipse Project Page](https://projects.eclipse.org/projects/technology.mdmbl)
* [mdmbl Eclipse Git Repositories](http://git.eclipse.org/c/?q=mdmbl)


## Version V0.7, 2017/07/21 ##

### API changes ###
  * [REQU-48](https://openmdm.atlassian.net/browse/REQU-48) - Type of Entity-IDs


### Changes ###
* [REQU-67](https://openmdm.atlassian.net/browse/REQU-48) - Final
* [REQU-92](https://openmdm.atlassian.net/browse/REQU-48) - Error Handling

### Bugzilla Bugs fixed ###
* [519448](https://bugs.eclipse.org/bugs/show_bug.cgi?id=) - Build of of freetextindexer in org.eclipse.mdm.nucleus fails
* [518062](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518062) - ODSAdapter: Encoding issue when switching to UTF-8
* [518060](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518060) - ODSAdapter - junit tests fail
* [515748](https://bugs.eclipse.org/bugs/show_bug.cgi?id=515748) - Unable to build org.eclipse.mdm.nucleus
* [518335](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518335) - Set executable flag for gradlew in git repo


## Version V0.6, 2017/06/07
### Changes ###

  * [REQU-2](https://openmdm.atlassian.net/browse/REQU-2) - Display a tree view for navigation
  * [REQU-3](https://openmdm.atlassian.net/browse/REQU-3) - Display icons in the tree view
  * [REQU-4](https://openmdm.atlassian.net/browse/REQU-4) - Display different ODS data sources in the tree view
  * [REQU-5](https://openmdm.atlassian.net/browse/REQU-5) - Expand serveral nodes of the tree view simultaneously    
  * [REQU-6](https://openmdm.atlassian.net/browse/REQU-6) - Display a scroll bar in the tree vie
  * [REQU-7](https://openmdm.atlassian.net/browse/REQU-7) - Web Client GUI Adjustment
  * [REQU-9](https://openmdm.atlassian.net/browse/REQU-9) - Display tabs on Detail view
  * [REQU-10](https://openmdm.atlassian.net/browse/REQU-10) - Update Detail View
  * [REQU-12](https://openmdm.atlassian.net/browse/REQU-12) - Select data source for attribute-based search
  * [REQU-13](https://openmdm.atlassian.net/browse/REQU-13) - Definition or selection of a search query
  * [REQU-14](https://openmdm.atlassian.net/browse/REQU-14) - Limit search to a certain result type
  * [REQU-15](https://openmdm.atlassian.net/browse/REQU-15) - Display attributes of the selected data source(s)
  * [REQU-16](https://openmdm.atlassian.net/browse/REQU-16) - Set search attribute values
  * [REQU-18](https://openmdm.atlassian.net/browse/REQU-18) - Select data source for fulltext search
  * [REQU-22](https://openmdm.atlassian.net/browse/REQU-22) - Create and store a view for search results
  * [REQU-23](https://openmdm.atlassian.net/browse/REQU-23) - Select a view to display search results
  * [REQU-24](https://openmdm.atlassian.net/browse/REQU-24) - Filter fulltext search results
  * [REQU-25](https://openmdm.atlassian.net/browse/REQU-25) - Display actions for search results
  * [REQU-27](https://openmdm.atlassian.net/browse/REQU-27) - Select data objects for shoppping basket
  * [REQU-28](https://openmdm.atlassian.net/browse/REQU-28) - Store a shopping basket
  * [REQU-29](https://openmdm.atlassian.net/browse/REQU-29) - Select a shopping basket
  * [REQU-30](https://openmdm.atlassian.net/browse/REQU-30) - Export shopping basket
  * [REQU-31](https://openmdm.atlassian.net/browse/REQU-31) - Load an exported shopping basket
  * [REQU-32](https://openmdm.atlassian.net/browse/REQU-32) - Display actions for shopping basket
  * [REQU-85](https://openmdm.atlassian.net/browse/REQU-85) - Seach type date
  * [REQU-86](https://openmdm.atlassian.net/browse/REQU-86) - Search across multiple data sources
  * [REQU-95](https://openmdm.atlassian.net/browse/REQU-95) - Backend configuration
