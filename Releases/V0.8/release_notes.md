# Release Notes - openMDM(R) Application #

* [mdmbl Eclipse Project Page](https://projects.eclipse.org/projects/technology.mdmbl)
* [mdmbl Eclipse Git Repositories](http://git.eclipse.org/c/?q=mdmbl)


## Version V0.8, 2017/09/08 ##

### API changes ###

  * [REQU-62](https://openmdm.atlassian.net/browse/REQU-62) - Polyvalant variants
  * [REQU-49](https://openmdm.atlassian.net/browse/REQU-49) - Extensibility of Entity Classes
  * [REQU-79](https://openmdm.atlassian.net/browse/REQU-79) - Consistent relationships
  * [REQU-97](https://openmdm.atlassian.net/browse/REQU-97) - Change handling of enumerations

### Changes ###

* [REQU-73](https://openmdm.atlassian.net/browse/REQU-73) - Representation class
* [REQU-74](https://openmdm.atlassian.net/browse/REQU-74) - Empty Interface
* [REQU-77](https://openmdm.atlassian.net/browse/REQU-77) - Name convention
* [REQU-82](https://openmdm.atlassian.net/browse/REQU-82) - Return-Type
* [REQU-91](https://openmdm.atlassian.net/browse/REQU-91) - Context Information

### Bugzilla Bugs fixed ###

* [520291](https://bugs.eclipse.org/bugs/show_bug.cgi?id=520291) - 	Elastic Search :: ElasticSearch answered 400, error while performing some search
* [521011](https://bugs.eclipse.org/bugs/show_bug.cgi?id=521011) - 	EntityFactory.createContextSensor always throws an exception
* [520330](https://bugs.eclipse.org/bugs/show_bug.cgi?id=520330) - 	Improve development setup in Eclipse
* [518063](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518063) - 	Nucleus: config-dir is missing in the build artefact mdm-web.zip
* [518124](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518124) - 	Configure JIPP and Sonar for mdmbl projects
* [518444](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518444) - 	Unify used gradle versions and update to latest stable
* [518825](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518825) - 	Nucleus build: create a separate gradle task for npm build
* [519212](https://bugs.eclipse.org/bugs/show_bug.cgi?id=519212) - 	Enable production mode for client build
* [519993](https://bugs.eclipse.org/bugs/show_bug.cgi?id=519993) - 	Create Gradle composite build
* [519453](https://bugs.eclipse.org/bugs/show_bug.cgi?id=519453) - 	org.eclipse.mdm.openatfx build can't download dependency
* [519995](https://bugs.eclipse.org/bugs/show_bug.cgi?id=519995) - 	Setup Guide and avalon
* [520248](https://bugs.eclipse.org/bugs/show_bug.cgi?id=520248) - 	Build of org.eclipse.mdm.api.odsadapter only works with "gradle clean install"
* [517057](https://bugs.eclipse.org/bugs/show_bug.cgi?id=517057) - 	Add Repository Descriptions



## Version V0.7, 2017/07/21 ##

### API changes ###
  * [REQU-48](https://openmdm.atlassian.net/browse/REQU-48) - Type of Entity-IDs


### Changes ###
* [REQU-67](https://openmdm.atlassian.net/browse/REQU-67) - Final
* [REQU-92](https://openmdm.atlassian.net/browse/REQU-92) - Error Handling

### Bugzilla Bugs fixed ###
* [519448](https://bugs.eclipse.org/bugs/show_bug.cgi?id=519448) - Build of of freetextindexer in org.eclipse.mdm.nucleus fails
* [518062](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518062) - ODSAdapter: Encoding issue when switching to UTF-8
* [518060](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518060) - ODSAdapter - junit tests fail
* [515748](https://bugs.eclipse.org/bugs/show_bug.cgi?id=515748) - Unable to build org.eclipse.mdm.nucleus
* [518335](https://bugs.eclipse.org/bugs/show_bug.cgi?id=518335) - Set executable flag for gradlew in git repo


## Version V0.6, 2017/06/07
### Changes ###

  * [REQU-2](https://openmdm.atlassian.net/browse/REQU-2) - Display a tree view for navigation
  * [REQU-3](https://openmdm.atlassian.net/browse/REQU-3) - Display icons in the tree view
  * [REQU-4](https://openmdm.atlassian.net/browse/REQU-4) - Display different ODS data sources in the tree view
  * [REQU-5](https://openmdm.atlassian.net/browse/REQU-5) - Expand serveral nodes of the tree view simultaneously    
  * [REQU-6](https://openmdm.atlassian.net/browse/REQU-6) - Display a scroll bar in the tree vie
  * [REQU-7](https://openmdm.atlassian.net/browse/REQU-7) - Web Client GUI Adjustment
  * [REQU-9](https://openmdm.atlassian.net/browse/REQU-9) - Display tabs on Detail view
  * [REQU-10](https://openmdm.atlassian.net/browse/REQU-10) - Update Detail View
  * [REQU-12](https://openmdm.atlassian.net/browse/REQU-12) - Select data source for attribute-based search
  * [REQU-13](https://openmdm.atlassian.net/browse/REQU-13) - Definition or selection of a search query
  * [REQU-14](https://openmdm.atlassian.net/browse/REQU-14) - Limit search to a certain result type
  * [REQU-15](https://openmdm.atlassian.net/browse/REQU-15) - Display attributes of the selected data source(s)
  * [REQU-16](https://openmdm.atlassian.net/browse/REQU-16) - Set search attribute values
  * [REQU-18](https://openmdm.atlassian.net/browse/REQU-18) - Select data source for fulltext search
  * [REQU-22](https://openmdm.atlassian.net/browse/REQU-22) - Create and store a view for search results
  * [REQU-23](https://openmdm.atlassian.net/browse/REQU-23) - Select a view to display search results
  * [REQU-24](https://openmdm.atlassian.net/browse/REQU-24) - Filter fulltext search results
  * [REQU-25](https://openmdm.atlassian.net/browse/REQU-25) - Display actions for search results
  * [REQU-27](https://openmdm.atlassian.net/browse/REQU-27) - Select data objects for shoppping basket
  * [REQU-28](https://openmdm.atlassian.net/browse/REQU-28) - Store a shopping basket
  * [REQU-29](https://openmdm.atlassian.net/browse/REQU-29) - Select a shopping basket
  * [REQU-30](https://openmdm.atlassian.net/browse/REQU-30) - Export shopping basket
  * [REQU-31](https://openmdm.atlassian.net/browse/REQU-31) - Load an exported shopping basket
  * [REQU-32](https://openmdm.atlassian.net/browse/REQU-32) - Display actions for shopping basket
  * [REQU-85](https://openmdm.atlassian.net/browse/REQU-85) - Seach type date
  * [REQU-86](https://openmdm.atlassian.net/browse/REQU-86) - Search across multiple data sources
  * [REQU-95](https://openmdm.atlassian.net/browse/REQU-95) - Backend configuration
