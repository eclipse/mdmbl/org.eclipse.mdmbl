## Introduction

This dockerfile should facilitate installation and allow us to have a common base for tests

## Prerequisites

You need to have a license server for avalon running (if you setup the server for the first time, please
keep in mind that the standard license key for avalon will not work on virtual machines, so double check if
you have a real machine as license server, before you request the licence file).

You also need to have an Oracle server installation running, which is compatible to the 
Oracle 12.2 instant client. You should have an openMDM 5 compatible database schema installed on 
the oracle server.

## Installation

you need to do the following steps:
1. download oracle instant client rpms from here: http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html
   You need the base-rpm and the sqlplus rpm. We recommend using the oracle 12.2 client.
2. put the oracle rpms in the oracle subdir
3. download the avalon linux binary from here: https://www.highqsoft.com/downloads
   You need the linux archive. We recommend avalon 4.6a with jacorb.
4. put the avalon archive into the avalon directory
5. edit or replace config/tnsnames.ora so that it contains the connection parameters for your 
oracle database.
6. edit config/athos_ini.diff . Just change the lines marked by "MINIMAL SETUP" to the settings necessary
   for your environment.
7. run "docker build --tag avalon docker-avalon"

## Running
Just run "docker run --net=host -ti avalon".
This will use port 2809 for the corba nameservice and port 43333 for the communication
with avalon. The ip adress used will be the address of the host docker is running on.
The name of the service will be "avods".

So an appropriate entry in your glassfish/domains/domain1/config/org.eclipse.mdm.connector/service.xml
would look like this (replace the IP by the one of your docker host):
<service entityManagerFactoryClass="org.eclipse.mdm.api.odsadapter.ODSEntityManagerFactory">
	<param name="nameservice">corbaloc::1.2@192.168.1.234:2809/NameService</param>
	<param name="servicename">avods.ASAM-ODS</param>
</service>

## Copyright and License ##
Copyright (c) 2015-2018 Contributors to the Eclipse Foundation

 See the NOTICE file(s) distributed with this work for additional
 information regarding copyright ownership.

 This program and the accompanying materials are made available under the
 terms of the Eclipse Public License v. 2.0 which is available at
 http://www.eclipse.org/legal/epl-2.0.

 SPDX-License-Identifier: EPL-2.0
